package hi.diyou.mc;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class dt implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger("dt");
	public static final ItemGroup OTHER = FabricItemGroupBuilder.create(
					new Identifier("dt", "something"))
			.icon(() -> new ItemStack(Items.KNOWLEDGE_BOOK))
			.appendItems((itemStacks, itemGroup) -> {
				itemStacks.add(new ItemStack(Blocks.COMMAND_BLOCK));
				itemStacks.add(new ItemStack(Blocks.STRUCTURE_BLOCK));
				itemStacks.add(new ItemStack(Blocks.BARRIER));
				itemStacks.add(new ItemStack(Blocks.JIGSAW));
				itemStacks.add(new ItemStack(Blocks.STRUCTURE_VOID));
				itemStacks.add(new ItemStack(Blocks.SPAWNER));

				itemStacks.add(ItemStack.EMPTY);

				itemStacks.add(new ItemStack(Items.KNOWLEDGE_BOOK));
				itemStacks.add(new ItemStack(Items.DEBUG_STICK));
			})
			.build();
	@Override
	public void onInitialize() {
		LOGGER.info("DT running");
	}
}
